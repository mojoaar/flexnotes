<?php namespace Flextype ?>
<?php use Flextype\Component\{Event\Event, Http\Http, Registry\Registry, Assets\Assets, Text\Text, Html\Html} ?>
    <?php Themes::view('partials/tail')->display() ?>
    <div class="powered">
        © <?php echo date("Y"); ?> <?= Html::toText(Registry::get('settings.title')) ?> | Made with <a href="http://flextype.org" target="_blank">Flextype</a> & ♥ | Managed by <a href="https://runcloud.io/r/KnZe2dxVYQjw" target="_blank">RunCloud.io</a>
    </div>
    </main>
  </body>
</html>
