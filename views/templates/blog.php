<?php namespace Flextype ?>
<?php Themes::view('partials/head')->display() ?>
<?php
$getEntries = Entries::getEntries('blog', 'date', 'ASC', 0, 10);
foreach($getEntries as $entry) {
    $visible = $entry['visibility'];
    if ($visible == 'visible') {
        $url = $entry['url'];
        $title = $entry['title'];
        $date = $entry['date'];
        $summary = $entry['summary'];
        echo "<a href='$url' class='blog-post'>";
        echo "<h3>$title</h3>";
        echo "<div>Published @ $date</div>";
        echo "<p>$summary</p>";

        $entryKeywords = $entry['keywords'];
        $entryKeywords = str_replace(' ', '', $entryKeywords);
        $keywordArray = explode (",", $entryKeywords);
        if (!empty($entryKeywords)) {
        echo "<div class='float-right'>Keywords: ";
        foreach($keywordArray as $word) {
            echo "#$word ";
        }
        echo "</div><br>";
        }
        echo "</a>";
    }
}
?>
<?php Themes::view('partials/footer')->display() ?>