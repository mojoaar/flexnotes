<?php namespace Flextype ?>
<?php use Flextype\Component\{Event\Event, Http\Http, Registry\Registry, Assets\Assets, Text\Text, Html\Html} ?>
<?php Themes::view('partials/head')->display() ?>
<h2><center><?= $entry['title'] ?></center></h2>
<!-- Start: Display sub-post information if not null (date/author) -->
<?php
    $entry = Entries::getCurrentEntry(); // get current blog post
    $entrydate = $entry['date']; // set the date variable
    $entryauthor = $entry['author']; // set the author variable
    echo "<center>";
    if (!empty($entrydate)) {
        echo "Published @ $entrydate";
    }
    if (!empty($entryauthor)) {
        echo " by $entryauthor";
    }
    echo "</center>";
?>
<!-- End: Display sub-post information if not null (date/author) -->
<div class="blog-post">
<?= $entry['content'] ?>
</div>
<a href="./" class="btn btn-outline-dark btn-sm" role="button">Back</a>
<!-- Start: Create Twitter share link -->
<?php
    $baseTweetUrl = "https://twitter.com/intent/tweet?url=";
    $entry = Entries::getCurrentEntry(); // get current blog post
    $entryUrl = $entry['url']; // set the url variable
    $entryUrl = str_replace(':', '%3A', $entryUrl); // replace ":" character
    $entryUrl = str_replace('/', '%2F', $entryUrl); // replace "/" character
    $entryKeywords = $entry['keywords']; // set the keywords variable
    $entryKeywords = str_replace(' ', '', $entryKeywords); // replace " " character
    $title = $entry['title']; // set the title variable
    $title = str_replace(' ', '+', $title); // replace " " character
    $endPoint = "$baseTweetUrl$entryUrl&text=$title&hashtags=$entryKeywords";
    echo "<a href='$endPoint' class='btn btn-outline-primary btn-sm' role='button' target='_blank'>Tweet</a>";
?>
<!-- End: Create Twitter share link -->
<!-- Start: Display keywords for blog post -->
<?php
    $entry = Entries::getCurrentEntry(); // get current blog post
    $entryKeywords = $entry['keywords']; // set the keywords variable
    $entryKeywords = str_replace(' ', '', $entryKeywords); // replace " " character
    $keywordArray = explode (",", $entryKeywords);
    if (!empty($entryKeywords)) {
    echo "<div class='float-right'>Keywords: ";
    foreach($keywordArray as $word) {
        echo "#$word ";
    }
    echo "</div>";
    }
?>
<!-- End: Display keywords for blog post -->
<?php Themes::view('partials/footer')->display() ?>
