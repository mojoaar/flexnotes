<?php namespace Flextype ?>
<?php Themes::view('partials/head')->display() ?>
<h2><?= $entry['title'] ?></h2>
<?= $entry['content'] ?>
<?php Themes::view('partials/footer')->display() ?>
