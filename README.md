# Flexnotes theme for [Flextype](http://flextype.org/)
![version](https://img.shields.io/badge/version-1.0.2-brightgreen.svg?style=flat-square)
![Flextype](https://img.shields.io/badge/Flextype-0.8.3-green.svg?style=flat-square)
![MIT License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)

![MIT License](flexnotes.png)  

## About
Flexnotes is a modified version of the default theme coming with Flextype CMS. It add's the following:
* Copyright year added to footer.
* Copyright site name added to footer.
* Date & author added to blog post if the post have them filled out.
* Back & Tweet button added to blog post.
* Prism.js added to handle codeblocks. Usage: `<pre><code class="language-css">p { color: red }</code></pre>`
* Keywords added to blog posts.
* Blog template corrected to only show post where visibility=visible (hide draft & hidden).
* TBD: more to come...

## Installation
* Download the theme.
* Unzip theme to the folder `/site/themes/`
* Go to `/site/config/settings.yaml` and update `theme:` setting with your theme name. Alternative go to Settings in the Admin panel and switch theme.
* Save your changes.

## Settings

```yaml
enabled: true # or `false` to disable the theme
```

## License
See [LICENSE](https://gitlab.com/mgjohansen/flexnotes/blob/master/LICENSE)